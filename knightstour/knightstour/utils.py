def chess2index(chess, boardsize):
	""" Convert Algebraic chess notation to internal index format """
	chess = chess.strip().lower()
	x = ord(chess[0]) - ord('a')
	y = boardsize - int(chess[1:])
	return (x, y)


def index2chess(x, y, boardsize):
	""" Convert Algebraic chess notation to internal index format """
	return chr(ord('a') + x) + str(boardsize - y)


def boardstring(board, boardsize):
	r = range(boardsize)
	lines = ''
	for y in r:
		lines += '\n' + ','.join('%2i' % board[(x, y)] if board[(x, y)] else '  '
								 for x in r)
	return lines