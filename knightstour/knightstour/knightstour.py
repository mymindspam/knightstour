import copy

from utils import chess2index, index2chess, boardstring

knight_moves = ((2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2), (1, -2), (2, -1))


def possible_moves(board, point, boardsize):
	""" Calculate possible moves """
	point_x, point_y = point
	kmoves = set((point_x + x, point_y + y) for x, y in knight_moves)
	kmoves = set((x, y)
				 for x, y in kmoves
				 if 0 <= x < boardsize
				 and 0 <= y < boardsize
				 and not board[(x, y)])
	return kmoves


def warnsdorff_rule_decision(board, point, boardsize):
	""" Choose cell with the lowest move possibilities """
	access = []
	brd = copy.deepcopy(board)
	for pos in possible_moves(board, point, boardsize=boardsize):
		brd[pos] = -1
		access.append((len(possible_moves(brd, pos, boardsize=boardsize)), pos))
		brd[pos] = 0
	return min(access)[1]


def knights_tour(point, boardsize):
	""" Calculate sequence of moves """
	board = {(x, y): 0 for x in range(boardsize) for y in range(boardsize)}
	move = 1
	board[point] = move
	moves = []
	moves.append(index2chess(*point, boardsize=boardsize))
	move += 1
	while move <= len(board):
		point = warnsdorff_rule_decision(board, point, boardsize)
		board[point] = move
		moves.append(index2chess(*point, boardsize=boardsize))
		move += 1
	return board, moves


def calc(boardsize, cell):
	boardsize = int(boardsize)
	if boardsize < 5:
		raise Exception()
	point = chess2index(cell, boardsize)
	board, moves = knights_tour(point, boardsize)
	return  moves, boardstring(board, boardsize=boardsize)
		