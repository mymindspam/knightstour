from django.shortcuts import render
from knightstour import calc

def index(request):
    args = {}
    if request.method == 'POST':
        boardsize = request.POST.get("boardsize")
        cell = request.POST.get("cell")
        args["moves"], args["boardstring"] = calc(boardsize, cell)
    return render(request, 'index.html', args)
